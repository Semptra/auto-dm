﻿using AutoDM.Scripts.Models;
using System.Collections.Generic;

namespace AutoDM.Scripts
{
    public interface IScriptGeneratorClient
    {
        IList<string> LoadTemplatePatternsForDatabase(Database database);

        GeneratedScript GenerateScript(string summary, Database database);
    }
}
