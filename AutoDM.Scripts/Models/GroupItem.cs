namespace AutoDM.Scripts.Models
{
    public class GroupItem
    {
        public string Name { get; set; }

        public string[] Values { get; set; }
    }
}