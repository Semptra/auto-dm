using System;
using System.Xml.Serialization;

namespace AutoDM.Scripts.Models
{
    [Serializable()]
    public class ScriptTemplate
    {
        [XmlIgnore()]
        public Database Database { get; set; }

        [XmlElement("Database")]
        public string DatabaseAsText
        {
            get
            {
                return Database.ToString();
            }
            set
            {
                if (value.Equals("Virgil", StringComparison.InvariantCultureIgnoreCase))
                {
                    Database = Database.Virgil;
                }
                else if (value.Equals("QCAT", StringComparison.InvariantCultureIgnoreCase))
                {
                    Database = Database.QCAT;
                }
                else
                {
                    throw new Exception($"Database \"{value}\" is not defined.");
                }
            }
        }

        [XmlElement("OrderIndex")]
        public int OrderIndex { get; set; }

        [XmlElement("Label")]
        public string Label { get; set; }

        [XmlElement("ParsePattern")]
        public string ParsePattern { get; set; }

        [XmlArray("RequiredVariables")]
        [XmlArrayItem("Variable", typeof(Variable))]
        public Variable[] Variables { get; set; }

        [XmlElement("EvidenceTemplate")]
        public string EvidenceTemplate { get; set; }

        [XmlElement("ScriptBodyTemplate")]
        public string ScriptBodyTemplate { get; set; }

        [XmlElement("ScriptBodyTestTemplate")]
        public string ScriptBodyTestTemplate { get; set; }

        [XmlElement("InputTableInsertTemplate")]
        public string InputTableInsertTemplate { get; set; }
    }
}
