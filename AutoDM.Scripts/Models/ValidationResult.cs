﻿namespace AutoDM.Scripts.Models
{
    public class ValidationResult
    {
        public int ExitCode { get; set; }

        public string Message { get; set; }

        public int AffectedRows { get; set; }
    }
}
