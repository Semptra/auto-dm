﻿using System.Collections.Generic;

namespace AutoDM.Scripts.Models
{
    public class GeneratedScriptPart
    {
        public int Index { get; set; }

        public ScriptTemplate Template { get; set; }

        public IList<string> InputVariables { get; set; }

        public IList<IList<string>> InputValues { get; set; }

        public string Evidence { get; set; }

        public string Body { get; set; }

        public string TestBody { get; set; }

        public string InsertStatements { get; set; }

        public GeneratedScriptPart(ScriptTemplate template)
        {
            Template = template;
        }
    }
}
