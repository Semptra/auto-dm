﻿using System.Collections.Generic;

namespace AutoDM.Scripts.Models
{
    public class GeneratedScript
    {
        public string Name { get; set; }

        public Database Database { get; set; }

        public IList<GeneratedScriptPart> GeneratedScriptParts { get; set; }

        public string Summary { get; set; }

        public string Script { get; set; }

        public string TestScript { get; set; }
    }
}
