using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

namespace AutoDM.Scripts.Models
{
    [Serializable()]
    public class Variable
    {
        [XmlElement("Name")]
        public string Name { get; set; }

        [XmlElement("Type")]
        public string Type { get; set; }

        [XmlIgnore()]
        public int? Size { get; set; }

        [XmlElement("Size")]
        public string SizeAsText
        {
            get
            {
                return (Size.HasValue) ? Size.ToString() : "null";
            }
            set
            {
                if (value.ToLower().Equals("null") || !Regex.IsMatch(value, @"^[0-9]+$"))
                {
                    Size = null;
                }
                else
                {
                    Size = int.Parse(value);
                }
            }
        }
    }

    public class VaribaleComparer : IEqualityComparer<Variable>
    {
        public bool Equals(Variable x, Variable y)
        {
            return x.Name.Equals(y.Name) && x.Type.Equals(y.Type) && x.SizeAsText.Equals(y.SizeAsText);
        }

        public int GetHashCode(Variable obj)
        {
            return obj.Name.GetHashCode() ^ obj.Type.GetHashCode() ^ obj.SizeAsText.GetHashCode();
        }
    }
}
