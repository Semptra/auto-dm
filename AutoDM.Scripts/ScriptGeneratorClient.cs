﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using AutoDM.Common.Configuration;
using AutoDM.Scripts.Models;

namespace AutoDM.Scripts
{
    public class ScriptGeneratorClient : IScriptGeneratorClient
    {
        private AppConfiguration configuration { get; }

        private ISummaryParser summaryParser { get; }

        private IScriptGenerator scriptGenerator { get; }

        private IScriptValidator scriptValidator { get; }

        public ScriptGeneratorClient(
            AppConfiguration configuration,
            ISummaryParser summaryParser,
            IScriptGenerator scriptGenerator,
            IScriptValidator scriptValidator)
        {
            this.configuration = configuration;
            this.summaryParser = summaryParser;
            this.scriptGenerator = scriptGenerator;
            this.scriptValidator = scriptValidator;
        }

        public IList<string> LoadTemplatePatternsForDatabase(Database database)
        {
            return this.summaryParser.LoadScriptTemplates()
                .Where(x => x.Database == database)
                .Select(x => x.ParsePattern)
                .ToList();
        }

        public GeneratedScript GenerateScript(string summary, Database database)
        {
            List<GeneratedScriptPart> generatedScriptParts = summaryParser.Parse(summary, database);

            if (generatedScriptParts.Count == 0)
            {
                throw new Exception("There are no script parts found");
            }

            GeneratedScript generatedScript = scriptGenerator.Generate(summary, generatedScriptParts);

            if (generatedScript != null)
            {
                ValidationResult validationResult = scriptValidator.Validate(generatedScript);

                if (validationResult.ExitCode == 0)
                {
                    generatedScript.Script = generatedScript.Script
                        .Replace("%ExpectedAffectedRows%", validationResult.AffectedRows.ToString());
                }
                else
                {
                    throw new Exception($"Validation Exception. Exit code: {validationResult.ExitCode}. Message: {validationResult.Message}");
                }
            }

            return generatedScript;
        }
    }
}
