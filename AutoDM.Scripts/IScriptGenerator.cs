﻿using AutoDM.Scripts.Models;
using System.Collections.Generic;

namespace AutoDM.Scripts
{
    public interface IScriptGenerator
    {
        GeneratedScript Generate(string summary, IEnumerable<GeneratedScriptPart> generatedScriptParts);
    }
}
