﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;
using AutoDM.Scripts.Models;
using AutoDM.Common.Configuration;
using AutoDM.Scripts.Extensions;

namespace AutoDM.Scripts
{
    public class ScriptGenerator : IScriptGenerator
    {
        private AppConfiguration configuration { get; }

        public ScriptGenerator(AppConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public GeneratedScript Generate(string summary, IEnumerable<GeneratedScriptPart> generatedScriptParts)
        {
            List<GeneratedScriptPart> orderedGeneratedScriptParts = generatedScriptParts.OrderBy(x => x.Template.OrderIndex).ToList();

            orderedGeneratedScriptParts = this.SetGeneratedScriptPartsIndexes(orderedGeneratedScriptParts);

            var scriptBodyTemplates = this.LoadScriptBodyTemplates();

            Database database = orderedGeneratedScriptParts.First().Template.Database;

            string scriptBodyProdTemplate = scriptBodyTemplates[Tuple.Create(database, Models.Environment.Prod)];
            string scriptBodyTestTemplate = scriptBodyTemplates[Tuple.Create(database, Models.Environment.Test)];

            var summaryBuilder = new StringBuilder();

            foreach (var summaryLine in summary.Split('\n'))
            {
                summaryBuilder.AppendTab();
                summaryBuilder.AppendWithNewLine(summaryLine);
            }

            string evidenceDeclaration = string.Join(System.Environment.NewLine,
                orderedGeneratedScriptParts.Select(GenerateScriptPartEvidenceDeclaration));

            string evidenceExecution = string.Join(System.Environment.NewLine,
                orderedGeneratedScriptParts.Select(GenerateScriptPartEvidenceExecution));

            string variablesDeclaration = string.Join(System.Environment.NewLine,
                orderedGeneratedScriptParts
                    .SelectMany(x => x.Template.Variables)
                    .Distinct(new VaribaleComparer())
                    .Select(GenerateVariableDeclaration));

            foreach (var generatedScriptPart in orderedGeneratedScriptParts)
            {
                GenerateInsertStatements(generatedScriptPart);
            }

            string productionStringBody = string.Join(System.Environment.NewLine,
                orderedGeneratedScriptParts.Select(GenerateProdScriptBody));

            string testStringBody = string.Join(System.Environment.NewLine,
                orderedGeneratedScriptParts.Select(GenerateTestScriptBody));

            return new GeneratedScript()
            {
                Name = GetGeneratedScriptName(database, summary),

                Database = database,

                GeneratedScriptParts = orderedGeneratedScriptParts.ToList(),

                Summary = summary,

                Script = string.Format(
                    scriptBodyProdTemplate,
                    summaryBuilder,
                    evidenceDeclaration,
                    evidenceExecution,
                    variablesDeclaration,
                    productionStringBody),

                TestScript = string.Format(
                    scriptBodyTestTemplate,
                    summaryBuilder,
                    evidenceDeclaration,
                    evidenceExecution,
                    variablesDeclaration,
                    testStringBody)
            };
        }

        private IDictionary<Tuple<Database, Models.Environment>, string> LoadScriptBodyTemplates()
        {
            return new Dictionary<Tuple<Database, Models.Environment>, string>
            {
                {
                    Tuple.Create(Database.Virgil, Models.Environment.Prod),
                    File.ReadAllText(this.configuration.Scripts.BodyTemplates.VirgilProd)
                },
                {
                    Tuple.Create(Database.Virgil, Models.Environment.Test),
                    File.ReadAllText(this.configuration.Scripts.BodyTemplates.VirgilTest)
                },
                {
                    Tuple.Create(Database.QCAT, Models.Environment.Prod),
                    File.ReadAllText(this.configuration.Scripts.BodyTemplates.QCATProd)
                },
                {
                    Tuple.Create(Database.QCAT, Models.Environment.Test),
                    File.ReadAllText(this.configuration.Scripts.BodyTemplates.QCATTest)
                }
            };
        }

        private List<GeneratedScriptPart> SetGeneratedScriptPartsIndexes(IEnumerable<GeneratedScriptPart> generatedScriptParts)
        {
            int i = 0;

            foreach (var generatedScriptPart in generatedScriptParts)
            {
                generatedScriptPart.Index = ++i;
            }

            return generatedScriptParts.ToList();
        }

        private string GetGeneratedScriptName(Database database, string summary)
        {
            string nameTemplate = database.ToString().ToLower() + "_{0}_rc1_update";

            var dmRegex = new Regex("DM[#]? [1-9][0-9]+");

            if (dmRegex.IsMatch(summary))
            {
                return string.Format(
                    nameTemplate,
                    dmRegex.Match(summary)
                        .Value
                        .Replace(" ", string.Empty)
                        .ToLower());
            }

            var lsRegex = new Regex("LS-[1-9][0-9]+");

            if (lsRegex.IsMatch(summary))
            {
                return string.Format(
                    nameTemplate,
                    lsRegex.Match(summary)
                        .Value
                        .Replace("-", string.Empty)
                        .ToLower());
            }

            return string.Format(nameTemplate, $"generated_script_{DateTime.Now}");
        }

        private string GenerateScriptPartEvidenceDeclaration(GeneratedScriptPart generatedScriptPart)
        {
            int index = generatedScriptPart.Index;

            string evidence = generatedScriptPart.Template.EvidenceTemplate;

            var evidenceVariables = generatedScriptPart.InputVariables
                .Where(x => generatedScriptPart.Template.EvidenceTemplate.Contains(x))
                .Select(
                    x => new
                    {
                        Index = generatedScriptPart.InputVariables.IndexOf(x),
                        Label = x
                    });

            foreach (var evidenceVariable in evidenceVariables)
            {
                string valuesString = string.Join(
                    ",",
                    generatedScriptPart.InputValues
                        .Select(x => x[evidenceVariable.Index].ToEvidenceString())
                        .Distinct());

                evidence = evidence.Replace(evidenceVariable.Label, valuesString);
            }

            var stringBuilder = new StringBuilder();

            stringBuilder.AppendWithNewLine($"DECLARE @sqlTestEvidenceQuery{index} VARCHAR(max)");
            stringBuilder.AppendWithNewLine($"SET @sqlTestEvidenceQuery{index} = '{evidence}'");

            generatedScriptPart.Evidence = stringBuilder.ToString();

            return generatedScriptPart.Evidence;
        }

        private string GenerateScriptPartEvidenceExecution(GeneratedScriptPart generatedScriptPart)
        {
            return $"EXEC sp_sqlexec @sqlTestEvidenceQuery{generatedScriptPart.Index}";
        }

        private string GenerateVariableDeclaration(Variable variable)
        {
            var stringBuilder = new StringBuilder();

            stringBuilder.AppendTab();
            stringBuilder.Append($"DECLARE @{variable.Name} {variable.Type}");

            if (variable.Size != null)
            {
                stringBuilder.Append($"({variable.Size})");
            }

            return stringBuilder.ToString();
        }

        private void GenerateInsertStatements(GeneratedScriptPart generatedScriptPart)
        {
            var inputVariables = generatedScriptPart.InputVariables
                .Where(x => generatedScriptPart.Template.InputTableInsertTemplate.Contains(x))
                .Select(
                    x => new
                    {
                        Index = generatedScriptPart.InputVariables.IndexOf(x),
                        Label = x
                    });

            var stringBuilder = new StringBuilder();

            foreach (var valuesList in generatedScriptPart.InputValues)
            {
                string insertStatement = generatedScriptPart.Template.InputTableInsertTemplate;

                foreach (var variable in inputVariables)
                {
                    insertStatement = insertStatement.Replace(variable.Label, valuesList[variable.Index].ToScriptString());
                }

                stringBuilder.AppendTab();
                stringBuilder.AppendWithNewLine(string.Format(insertStatement, generatedScriptPart.Index));
            }

            generatedScriptPart.InsertStatements = stringBuilder.ToString();
        }

        private string GenerateProdScriptBody(GeneratedScriptPart generatedScriptPart)
        {
            return string.Format(generatedScriptPart.Template.ScriptBodyTemplate, generatedScriptPart.Index, generatedScriptPart.InsertStatements);
        }

        private string GenerateTestScriptBody(GeneratedScriptPart generatedScriptPart)
        {
            return string.Format(generatedScriptPart.Template.ScriptBodyTestTemplate, generatedScriptPart.Index, generatedScriptPart.InsertStatements);
        }
    }
}