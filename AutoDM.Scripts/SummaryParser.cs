using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using AutoDM.Scripts.Extensions;
using AutoDM.Scripts.Models;
using AutoDM.Common.Configuration;

namespace AutoDM.Scripts
{
    public class SummaryParser : ISummaryParser
    {
        private AppConfiguration configuration { get; }

        public SummaryParser(AppConfiguration configuration)
        {
            this.configuration = configuration;
        }    

        public List<GeneratedScriptPart> Parse(string summary, Database database)
        {
            IEnumerable<string> summaryLines = summary.SplitToLines();

            IList<ScriptTemplate> scriptTemplates = this.LoadScriptTemplates()
                    .Where(x => x.Database == database)
                    .ToList();

            var generatedScriptParts = new List<GeneratedScriptPart>();

            foreach (string summaryLine in summaryLines)
            {
                ScriptTemplate scriptTemplate = scriptTemplates.FirstOrDefault(
                    x => Regex.IsMatch(
                            summaryLine, 
                            x.ParsePattern, 
                            RegexOptions.IgnoreCase | RegexOptions.CultureInvariant));

                if (scriptTemplate is null)
                {
                    continue;
                }

                var generatedScriptPart = generatedScriptParts.Any(x => x.Template == scriptTemplate) ?
                    generatedScriptParts.First(x => x.Template == scriptTemplate) :
                    new GeneratedScriptPart(scriptTemplate);

                generatedScriptPart.AddInputVariablesFromString(scriptTemplate.ParsePattern, summaryLine);

                if (!generatedScriptParts.Contains(generatedScriptPart))
                {
                    generatedScriptParts.Add(generatedScriptPart);
                }
            }

            return generatedScriptParts;
        }

        public IList<ScriptTemplate> LoadScriptTemplates()
        {
            string templatesDirectory = this.configuration.Scripts.TemplatesDirectory;

            if (!Directory.Exists(templatesDirectory))
            {
                throw new DirectoryNotFoundException(templatesDirectory);
            }

            var scriptTemplates = new List<ScriptTemplate>();
            var serializer = new XmlSerializer(typeof(ScriptTemplate));

            foreach (var filePath in Directory.EnumerateFiles(templatesDirectory, "*.xml"))
            {
                using (var streamReader = new StreamReader(filePath))
                {
                    scriptTemplates.Add((ScriptTemplate)serializer.Deserialize(streamReader));
                }
            }

            for (int i = 0; i < scriptTemplates.Count; i++)
            {
                scriptTemplates[i].ParsePattern = scriptTemplates[i].ParsePattern
                   .Trim(' ', '\t', '\n', '\r')
                   .Replace('\n', ' ');

                scriptTemplates[i].ParsePattern = Regex.Replace(scriptTemplates[i].ParsePattern, @"[\s]+", " ");
            }

            return scriptTemplates;
        }
    }
}