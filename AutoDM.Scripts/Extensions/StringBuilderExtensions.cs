﻿using System.Text;

namespace AutoDM.Scripts.Extensions
{
    public static class StringBuilderExtensions
    {
        public static void AppendWithNewLine(this StringBuilder stringBuilder, string value = null)
        {
            if (value != null)
            {
                stringBuilder.Append(value);
            }
            
            stringBuilder.Append("\n");
        }

        public static void AppendTab(this StringBuilder stringBuilder)
        {
            stringBuilder.Append("    ");
        }

        public static void RemoveFromEnd(this StringBuilder stringBuilder, int length)
        {
            stringBuilder.Remove(stringBuilder.Length - length, length);
        }
    }
}
