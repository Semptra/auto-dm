using System.Collections.Generic;
using System.Linq;

namespace AutoDM.Scripts.Extensions
{
    public static class ListExtensions
    {
        public static IEnumerable<IList<string>> GetGroupItemsPermutations(this List<List<string>> groupItems)
        {
            int[] indexes = new int[groupItems.Count];
            int maxCount = groupItems.Aggregate(1, (product, groupItem) => product *= groupItem.Count);

            for (int i = 0; i < maxCount; i++)
            {
                var currentResult = indexes.Select(_ => string.Empty).ToList();

                for (int j = 0; j < indexes.Length; j++)
                {
                    currentResult[j] = groupItems[j][indexes[j]];
                }

                yield return currentResult.ToList();

                for (int j = indexes.Length - 1; j >= 0; j--)
                {
                    indexes[j] = (indexes[j] + 1) % groupItems[j].Count;

                    if (!(indexes[j] == 0))
                    {
                        break;
                    }
                }
            }
        }
    }
}