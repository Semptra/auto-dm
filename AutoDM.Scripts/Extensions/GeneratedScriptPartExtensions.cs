﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using AutoDM.Scripts.Models;

namespace AutoDM.Scripts.Extensions
{
    public static class GeneratedScriptPartExtensions
    {
        public static void AddInputVariablesFromString(
            this GeneratedScriptPart generatedScriptPart, string parsePattern, string str)
        {
            Match match = new Regex(parsePattern).Match(str);

            IEnumerable<Group> matchedGroups = match.Groups
                .Cast<Group>()
                .Where(group => !int.TryParse(group.Name, out _));

            if (generatedScriptPart.InputVariables is null || 
                generatedScriptPart.InputVariables.Count == 0)
            {
                generatedScriptPart.InputVariables = matchedGroups.Select(x => x.Name).ToList();
            }

            List<List<string>> matchedGroupValues = matchedGroups.Select(group => 
                group.Value
                    .Split(',')
                    .Select(x => x.Trim())
                    .ToList())
                .ToList();

            if (generatedScriptPart.InputValues == null)
            {
                generatedScriptPart.InputValues = new List<IList<string>>();
            }

            foreach (var inputValues in matchedGroupValues.GetGroupItemsPermutations())
            {
                generatedScriptPart.InputValues.Add(inputValues);
            }
        }
    }
}
