using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AutoDM.Scripts.Extensions
{
    public static class StringExtensions
    {
        public static IEnumerable<string> SplitToLines(this string value)
        {
            return value.Split('\n')
                .Select(line => line.Trim())
                .Where(line => !string.IsNullOrEmpty(line));
        }

        public static string ToEvidenceString(this string value)
        {
            return string.Format("''{0}''", value);
        }

        public static string ToScriptString(this string value)
        {
            if (value.ToLower().Equals("null"))
            {
                return "null";
            }

            return Regex.IsMatch(value, @"^[1-9][0-9]*$") ? value : string.Format("'{0}'", value);
        }

        public static bool Contains(this string str, string value, StringComparison comparison = StringComparison.InvariantCultureIgnoreCase)
        {
            return str.IndexOf(value, comparison) >= 0;
        }
    }
}