﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using AutoDM.Common.Configuration;
using AutoDM.Scripts.Models;

namespace AutoDM.Scripts
{
    public class ScriptValidator : IScriptValidator
    {
        private AppConfiguration configuration { get; }

        public ScriptValidator(AppConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public ValidationResult Validate(GeneratedScript generatedScript)
        {
            string connectionString;

            switch (generatedScript.Database)
            {
                case Database.Virgil:
                    connectionString = this.configuration.ConnectionStrings.Virgil;
                    break;
                case Database.QCAT:
                    connectionString = this.configuration.ConnectionStrings.QCAT;
                    break;
                default:
                    throw new Exception("There are no Database specified");
            }

            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var command = new SqlCommand(generatedScript.TestScript, connection);

                return ExecuteCommand(command);
            }
        }
        
        private ValidationResult ExecuteCommand(SqlCommand command)
        {
            var result = new ValidationResult
            {
                ExitCode = 0,
                Message = null,
                AffectedRows = -1
            };

            try
            {
                using (var reader = command.ExecuteReader())
                {
                    do
                    {
                        IEnumerable<string> names = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName);

                        if (names.SingleOrDefault(x => x == "ActualAffectedRows") != null)
                        {
                            IDataRecord affectedRowsRecord = (reader as IEnumerable).Cast<IDataRecord>().Last();

                            int affectedRows = int.Parse(affectedRowsRecord[names.Single()].ToString());

                            // Add additional rows just for the safety.
                            result.AffectedRows = affectedRows + 1 + affectedRows / 5;
                        }
                    }
                    while (reader.NextResult());
                }
            }
            catch(SqlException ex)
            {
                result.ExitCode = ex.ErrorCode;
                result.Message = ex.Message;
            }

            return result;
        }
    }
}
