using AutoDM.Scripts.Models;
using System.Collections.Generic;

namespace AutoDM.Scripts
{
    public interface ISummaryParser
    {
        List<GeneratedScriptPart> Parse(string summary, Database database);

        IList<ScriptTemplate> LoadScriptTemplates();
    }
}