﻿using AutoDM.Scripts.Models;

namespace AutoDM.Scripts
{
    public interface IScriptValidator
    {
        ValidationResult Validate(GeneratedScript generatedScript);
    }
}
