﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Input;
using System.Windows.Controls;
using Autofac;
using Newtonsoft.Json;
using AutoDM.Common.Configuration;
using AutoDM.Scripts;
using AutoDM.Scripts.Models;
using AutoDM.WPFClient.Extensions;
using Microsoft.Win32;

namespace AutoDM.WPFClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public AppConfiguration configuration { get; }

        private IScriptGeneratorClient scriptClient { get; }

        private IEnumerable<string> parsePatterns { get; set; }

        private IEnumerable<Brush> brushes { get; }

        private Database selectedDatabase { get; set; }

        private GeneratedScript generatedScript { get; set; } = null;

        public MainWindow()
        {
            var container = this.BuildContainer();

            this.configuration = container.Resolve<AppConfiguration>();
            this.scriptClient = container.Resolve<IScriptGeneratorClient>();

            InitializeComponent();

            this.brushes = this.configuration.WPF.Brushes.Highlighs.Select(x => Parse(x.Color));

            this.SummaryTextBox.Document.PageWidth = 2000;
            this.ScriptTextBox.Document.PageWidth = 2000;
        }

        private IContainer BuildContainer()
        {
            var builder = new ContainerBuilder();

            AppConfiguration configuration = JsonConvert.DeserializeObject<AppConfiguration>(
                File.ReadAllText(
                    Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json")));

            // Configurations

            builder.RegisterInstance(configuration);

            // Clients

            builder.RegisterType<SummaryParser>().As<ISummaryParser>();
            builder.RegisterType<ScriptValidator>().As<IScriptValidator>();
            builder.RegisterType<ScriptGenerator>().As<IScriptGenerator>();
            builder.RegisterType<ScriptGeneratorClient>().As<IScriptGeneratorClient>();

            return builder.Build();
        }
  
        #region Events

        private async void HighlightButton_Click(object sender, RoutedEventArgs e)
        {
            await this.HighlightText();
        }

        private async void GenerateButton_Click(object sender, RoutedEventArgs e)
        {
            await this.HighlightText();
            await this.GenerateScript();
        }

        private void SaveScriptButton_Click(object sender, RoutedEventArgs e)
        {
            if(this.generatedScript != null)
            {
                this.generatedScript.Script = this.ScriptTextBox.Document.GetText();

                var saveScriptDialog = new SaveFileDialog
                {
                    FileName = this.generatedScript.Name,
                    Filter = "Scripts|*.script|All files|*.*",
                    InitialDirectory = this.configuration.Scripts.GeneratedScriptsDirectory
                };

                if (saveScriptDialog.ShowDialog() == true)
                {
                    File.WriteAllText(saveScriptDialog.FileName, this.generatedScript.Script);
                }    
            } 
        }

        private void DatabaseComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedDatabaseString = (this.DatabaseComboBox.SelectedItem as ComboBoxItem).Content.ToString();

            this.selectedDatabase = (Database)Enum.Parse(typeof(Database), selectedDatabaseString, true);
            this.parsePatterns = this.scriptClient.LoadTemplatePatternsForDatabase(this.selectedDatabase);
        }

        #endregion

        private async Task HighlightText()
        {
            await this.SummaryTextBox.Dispatcher.InvokeAsync(() =>
            {
                var highlightedBlocks = this.SummaryTextBox.Document.HighlightText(
                    this.parsePatterns, this.brushes);

                this.SummaryTextBox.BeginChange();

                this.SummaryTextBox.Document.Blocks.Clear();
                this.SummaryTextBox.Document.Blocks.AddRange(highlightedBlocks);

                this.SummaryTextBox.EndChange();

                this.SummaryTextBox.Focus();

                this.SummaryTextBox.CaretPosition = this.SummaryTextBox.Document.ContentEnd;
            });
        }

        private async Task GenerateScript()
        {
            string summary = this.SummaryTextBox.Document.GetText();

            await Task.Run(() =>
            {
                try
                {
                    this.ScriptLoadingSpinner.Dispatcher.InvokeAsync(() => 
                    {
                        this.ScriptLoadingSpinner.Visibility = Visibility.Visible;
                        this.ScriptLoadingSpinner.Spin = true;
                    });

                    this.ScriptTextBox.Dispatcher.InvokeAsync(() => this.ScriptTextBox.IsEnabled = false);

                    this.generatedScript = this.scriptClient.GenerateScript(summary, this.selectedDatabase);

                    this.ScriptTextBox.Dispatcher.InvokeAsync(() =>
                    {
                        this.ScriptTextBox.Document.Blocks.Clear();
                        this.ScriptTextBox.AppendText(this.generatedScript.Script);
                    });
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK);
                }
                finally
                {
                    this.ScriptLoadingSpinner.Dispatcher.InvokeAsync(() =>
                    {
                        this.ScriptLoadingSpinner.Visibility = Visibility.Hidden;
                        this.ScriptLoadingSpinner.Spin = false;
                    });

                    this.ScriptTextBox.Dispatcher.InvokeAsync(() => this.ScriptTextBox.IsEnabled = true);
                }
            });
        }

        private Brush Parse(string color)
        {
            var brushConverter = new BrushConverter();

            return (SolidColorBrush)brushConverter.ConvertFrom(color);
        }
    }
}
