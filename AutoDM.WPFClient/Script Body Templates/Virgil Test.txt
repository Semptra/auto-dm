USE StrikeforceDB
SET NOCOUNT ON
SET QUOTED_IDENTIFIER ON

-- Summary
/*
{0}
*/

-- Evidences declaration
{1}
-- Evidences execution
SELECT '<================= Evidences =================>'
{2}

	-- Variables declaration
	DECLARE @ActualAffectedRows INT = 0
	
	DECLARE @PersonID UNIQUEIDENTIFIER = (SELECT p.PersonID FROM Person AS p WHERE p.FirstName = 'Data' AND p.LastName = 'Administrator')
	DECLARE @ThrowErrorMessage NVARCHAR(MAX)	
	
	{3} 
	-- Script bodies
    {4}
   
SELECT @ActualAffectedRows AS ActualAffectedRows