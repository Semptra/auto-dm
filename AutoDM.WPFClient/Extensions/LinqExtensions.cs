﻿using System.Collections.Generic;

namespace AutoDM.WPFClient.Extensions
{
    public static class LinqExtensions
    {
        public static IEnumerable<T> SkipLast<T>(this IEnumerable<T> source, int count = 1)
        {
            var it = source.GetEnumerator();
            bool hasRemainingItems = false;
            var cache = new Queue<T>(count + 1);

            do
            {
                if (hasRemainingItems = it.MoveNext())
                {
                    cache.Enqueue(it.Current);

                    if (cache.Count > count)
                        yield return cache.Dequeue();
                }
            }
            while (hasRemainingItems);
        }
    }
}
