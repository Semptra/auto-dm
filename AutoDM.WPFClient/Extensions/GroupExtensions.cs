﻿using System.Text.RegularExpressions;

namespace AutoDM.WPFClient.Extensions
{
    public static class GroupExtensions
    {
        private static Regex NumberRegex = new Regex("[0-9]+",
            RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);

        public static bool IsNamed(this Group group)
        {
            bool isMatch = NumberRegex.IsMatch(group.Name);
            return !NumberRegex.IsMatch(group.Name);
        }
    }
}
