﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Linq;
using System.Windows.Documents;
using System.Windows.Media;

namespace AutoDM.WPFClient.Extensions
{
    public static class FlowDocumentExtensions
    {
        static Random rnd = new Random();

        public static IEnumerable<string> GetAllLines(this FlowDocument document)
        {
            var textRange = new TextRange(document.ContentStart, document.ContentEnd);

            return textRange.Text.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
        }

        public static string GetText(this FlowDocument document)
        {
            return new TextRange(document.ContentStart, document.ContentEnd).Text;
        }

        public static IEnumerable<Block> HighlightText(this FlowDocument document, 
            IEnumerable<string> patterns, IEnumerable<Brush> brushes)
        {
            return HighlightText(document, patterns.Select(x => new Regex(x, RegexOptions.CultureInvariant | RegexOptions.IgnoreCase)), brushes);
        }

        public static IEnumerable<Block> HighlightText(this FlowDocument document,
            IEnumerable<Regex> regexes, IEnumerable<Brush> brushes)
        {
            var summaryLines = document.GetAllLines().Select(x => x.TrimEnd());

            var blocks = new List<Block>();

            foreach (string summaryLine in summaryLines)
            {
                var regex = regexes.SingleOrDefault(x => x.IsMatch(summaryLine));

                if (regex is null)
                {
                    var block = new Span();
                    block.Inlines.Add(summaryLine);
                    blocks.Add(new Paragraph(block));

                    continue;
                }

                var match = regex.Match(summaryLine);

                var groups = match.Groups
                    .OfType<Group>()
                    .OrderBy(x => x.Index)
                    .Skip(1);

                var currentLine = new Span();

                int brushIndex = 0;
                foreach (var group in groups)
                {
                    var span = new Span();
                    span.Inlines.Add(group.Value);

                    if (group.IsNamed())
                    {
                        span.Background = brushes.ElementAtOrDefault(brushIndex++) ?? PickRandomBrush();
                    }

                    currentLine.Inlines.Add(span);
                }

                blocks.Add(new Paragraph(currentLine));
            }

            return blocks.SkipLast();
        }

        private static Brush PickRandomBrush()
        {
            Brush brush = Brushes.Transparent;

            Type brushesType = typeof(Brushes);

            PropertyInfo[] properties = brushesType.GetProperties();

            int random = rnd.Next(properties.Length);
            brush = (Brush)properties[random].GetValue(null, null);

            return brush;
        }
    }
}
