﻿using System;
using System.Windows.Documents;

namespace AutoDM.WPFClient.Extensions
{
    public static class TextPointerExtensions
    {

        public static TextPointer CorrectPosition(this TextPointer position, string word, bool caseSensitive = false)
        {
            TextPointer start = null;
            while (position != null)
            {
                if (position.GetPointerContext(LogicalDirection.Forward) == TextPointerContext.Text)
                {
                    string textRun = position.GetTextInRun(LogicalDirection.Forward);

                    int indexInRun = textRun.IndexOf(word, caseSensitive ? StringComparison.InvariantCulture : StringComparison.InvariantCultureIgnoreCase);
                    if (indexInRun >= 0)
                    {
                        start = position.GetPositionAtOffset(indexInRun);
                        break;
                    }
                }

                position = position.GetNextContextPosition(LogicalDirection.Forward);
            }

            return start;
        }
    }
}
