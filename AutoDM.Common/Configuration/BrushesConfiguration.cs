﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace AutoDM.Common.Configuration
{
    public class BrushesConfiguration
    {
        public string ForegroundDefault { get; set; }

        public string BackgroundDefault { get; set; }

        [JsonProperty("Highlights")]
        public ICollection<BrushSetting> Highlighs { get; set; }
    }
}
