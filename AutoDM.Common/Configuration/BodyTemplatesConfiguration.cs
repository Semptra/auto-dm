﻿namespace AutoDM.Common.Configuration
{
    public class BodyTemplatesConfiguration
    {
        public string VirgilProd { get; set; }

        public string VirgilTest { get; set; }

        public string QCATProd { get; set; }

        public string QCATTest { get; set; }
    }
}
