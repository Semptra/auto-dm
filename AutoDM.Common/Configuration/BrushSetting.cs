﻿namespace AutoDM.Common.Configuration
{
    public class BrushSetting
    {
        public int Index { get; set; }

        public string Color { get; set; }
    }
}
