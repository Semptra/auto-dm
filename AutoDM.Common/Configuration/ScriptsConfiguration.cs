﻿namespace AutoDM.Common.Configuration
{
    public class ScriptsConfiguration
    {
        public string TemplatesDirectory { get; set; }

        public string GeneratedScriptsDirectory { get; set; }

        public BodyTemplatesConfiguration BodyTemplates { get; set; }
    }
}
