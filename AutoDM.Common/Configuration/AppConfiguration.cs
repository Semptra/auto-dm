﻿namespace AutoDM.Common.Configuration
{
    public class AppConfiguration
    {
        public ConnectionStringsConfiguration ConnectionStrings { get; set; }

        public ScriptsConfiguration Scripts { get; set; }

        public WPFConfiguration WPF { get; set; }
    }
}
