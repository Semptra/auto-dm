# Auto DM
Auto DM - automatization for Data Management scripts generation.

## List of supported scripts

* **Virgil**
    * Add Scale
    * Add Site
    * Move Audio Recording
    * Move Scale
    * Move Survey
    * Move SVID
    * Remove Audio
    * Remove Scale
    * Remove Subject
    * Remove SVID
    * Swap Scales
    * Transfer Subject
    * Update CulureTemplate Version
    * Update Feedback Status for Scale
    * Update PdfRegenerationHistoryID for Scale
    * Update PI for Site
    * Update Rater for Scale
    * Update Rater for Scale Revision
    * Update Scale Revision StartTime
    * Update Screening and Randomization Number
    * Update Subject and Randomization Number
    * Update Subject and Screening Number
    * Update Survey Email
    * Update Visit Date
    * Update Visit Name
* **QCAT**
    * Move SVID
    * Remove SVID
    * Update CDR ReviewType
    * Update MMSE ReviewType
    * Update ReviewType
    * Update Visit Name

## Prerequisite
* .NET Framework v4.7.2 or later

## Downloads
* [Release v1.0](https://gitlab.com/Semptra/auto-dm/tags/v1.0)

## References
* [MedAvante Confluence](https://medavanteinc.atlassian.net/wiki/spaces/LS/pages/77725700/Data+Change+Requests+DCF)
* [T-SQL Syntax](https://docs.microsoft.com/en-us/sql/t-sql/queries/queries?view=sql-server-2017)
* [WPF Get Started](https://docs.microsoft.com/en-us/dotnet/framework/wpf/getting-started/walkthrough-my-first-wpf-desktop-application)
* [WPF Material Desing](https://github.com/MaterialDesignInXAML/MaterialDesignInXamlToolkit)

## Screenshots

### Main window
![AutoDM main window](img/auto_dm_main_window.PNG)

### In action
![AutoDM in action](img/auto_dm_in_action.PNG)